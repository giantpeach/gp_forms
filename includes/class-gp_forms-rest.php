<?php
use Respect\Validation\Validator as Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use ReCaptcha\ReCaptcha;

class Gp_forms_Rest {

    public function __construct()
    {
        $this->namespace = "gp_forms/v1";

        if (file_exists(realpath(dirname(__FILE__)).'/../../../../../vendor/autoload.php')) {
            require_once realpath(dirname(__FILE__)).'/../../../../../vendor/autoload.php';
        }
        else if (file_exists(realpath(dirname(__FILE__)).'/../../vendor/autoload.php')) {
            require_once realpath(dirname(__FILE__)).'/../../vendor/autoload.php';
        }
        
        include get_template_directory() . '/gp_forms/forms.php';
        include "class-gp_forms-mailchimp.php";

        $this->forms = $forms;
        
        $this->mailchimp = new Gp_forms_Mailchimp('gp_forms', '1.0.0');
        
        if (!empty($googleRecaptchaSecretKey)) { // defined in /gp_forms/forms.php
            $this->googleRecaptchaSecretKey = $googleRecaptchaSecretKey;
        }

        add_action( 'rest_api_init', [$this, 'register_routes'] );
    }

    public function register_routes() 
    {
        register_rest_route( $this->namespace , '/submit/(?P<form>\S+)', array(
            'methods' => 'POST',
            'callback' => array( $this, 'form_submit' ),
            'permission_callback' => '__return_true'
        ));
    }

    public function form_submit( $request ) 
    {
        if ( !check_ajax_referer( 'wp_rest', '_wpnonce' ) ) {
            return 'lol';
        } else {

            $mailchimp = false;
            $notificationEmail = false;
            $sendDefaultAdminNotification = true;
            $redirect = false;
            $redirect_new_tab = false;
            $currentForm = "";
            $formSettings = false;
            $googleRecaptcha = false;

            foreach ( $this->forms as $form ) {
                if ( $form['form_slug'] == $request['form'] ) {
                    $formSettings = $form;
                    $formName = $form['form_name'];
                    $formSlug = $form['form_slug'];
                    $tmpFields = $form['fields'];
                    $tmpValidation = $form['validation'];
                    $tmpErrors = $form['errors'];
                    $errorMessage = $form['mainError'];
                    if (isset($form['sendDefaultAdminNotification'])) {
                        $sendDefaultAdminNotification = $form['sendDefaultAdminNotification'];
                    }

                    if ( isset( $form['files'] ) ) {
                        $mimetypes = $form['files'];
                    } else {
                        $mimetypes = false;
                    }

                    if ( isset( $form['filesize'] ) ) {
                        $filesize = $form['filesize'];
                    } else {
                        $filesize = false;
                    }

                    if ( isset( $form['successMessage'] ) ) {
                        $successMessage = $form['successMessage'];
                    } else {
                        $successMessage = 'Form submitted successfully.';
                    }

                    if ( isset( $form['redirect'] ) ) {
                        $redirect = $form['redirect'];                                
                    }

                    if ( isset( $form['redirect_new_tab'] ) ) {
                        $redirect_new_tab = $form['redirect_new_tab'];                                
                    }

                    if ( !isset( $form['save_submission'] ) ) {
                        $save_submission = true;                                
                    }
                    else {
                        $save_submission = $form['save_submission'];
                    }
                    
                    if ( isset( $form['email'] ) && !empty( $form['email'] ) ) {
                        $notificationEmail = $form['email'];                                
                    }
                    if ( empty($notificationEmail) ) {
                        $notificationEmail = get_option( 'admin_email' );
                    }

                    if ( isset( $form['from'] ) && !empty( $form['from']['email'] ) && !empty( $form['from']['name'] ) ) {
                        $from = array(
                            'email' => $form['from']['email'],
                            'name' => $form['from']['name']
                        );
                    } else {
                        $from = array(
                            'email' => get_option( 'admin_email' ),
                            'name' => get_bloginfo( 'name' )
                        );
                    }

                    if (isset($form['cc']) && !empty($form['cc'])) {
                        $cc = $form['cc'];
                    } else {
                        $cc = "";
                    }

                    if (isset($form['mailchimp']) && $form['mailchimp']['enabled'] && $form['mailchimp']['list_id'] != "") {
                        $mailchimp = true;
                    }

                    if (!empty($form['google_recaptcha'])) {
                        $googleRecaptcha = true;
                    }

                    $currentForm = $form;
                }
            }

            $formData = filter_input_array(
                INPUT_POST,
                $tmpFields
            );

            // Handle checkbox groups
            foreach ($formData as $key => $data) {
                if ($data === false) {
                    if (isset($_POST[$key]) && is_array($_POST[$key])) {
                        $value = implode(", ",$_POST[$key]);
                        $formData[$key] = filter_var($value, FILTER_SANITIZE_STRING);
                    }
                }
            }

            //+----------------------------------------------------------------
            //| Google reCaptcha
            //+----------------------------------------------------------------
            if ($googleRecaptcha === true) {
                if (!empty($this->googleRecaptchaSecretKey)) {
                    if (!$this->checkRecaptcha($formData['g-recaptcha-response'], $this->getRemoteIp())) {
                        return 'invalid recaptcha response';
                    } else {
                        unset($formData['g-recaptcha-response']);
                    }
                }
            }

            // We're checking if a field name is passed through as the notification email
            // if so, get the value
            if ( array_key_exists( $notificationEmail, $formData ) ) {
                $notificationEmail = $formData[$notificationEmail];
            }

            $notificationEmail = apply_filters('gp_forms_notification_email', $notificationEmail, $formData, $formSettings);
            $redirect = apply_filters('gp_forms_redirect', $redirect, $formData, $formSettings);
            
            try {
                $tmpValidation->assert($formData);

                $files = null;
                
                if ( isset( $_FILES ) && !empty( $_FILES ) ) {
                    $fileFound = false;
                    foreach ($_FILES as $input => $_file) {
                        if (!empty($_file['name'])) {
                            $fileFound = true;
                            break;
                        }
                    }
                    if ($fileFound) {
                        $files = $this->process_files( $_FILES, $mimetypes, $filesize );
                        if (!$files) {
                            throw new NestedValidationException('Invalid file');
                        }
                    }
                }

                $custom_errors = apply_filters('gp_forms_custom_validation', array(), $formData, $formName, $formSettings);

                if (!empty($custom_errors)) {
                    if (isset($custom_errors['mainError'])) {
                        $errorMessage = $custom_errors['mainError'];
                        unset($custom_errors['mainError']);
                    };
                    return array (
                        'hasErrors' => true,
                        'errors' => $custom_errors['errors'],
                        'mainError' => $errorMessage
                    );
                }

                $save_submission = apply_filters('gp_forms_should_save_data', $save_submission, $formData, $formName, $formSettings);

                $formData = apply_filters('gp_forms_form_data', $formData, $formName, $formSettings);

                if ($save_submission) {
                    $this->process_data( $formData, $tmpFields, $formSlug );
                }

                if ($sendDefaultAdminNotification) {
                    $this->notify_admin( $formData, $formName, $formSettings, $notificationEmail, $from, $files, $cc );
                }

                do_action('gp_forms_after_form_save', $formData, $formName, $formSettings, array( 'notificationEmail' => $notificationEmail, 'from' => $from, 'files' => $files, 'cc' => $cc));

                $success_html = apply_filters('gp_forms_success_html','',$formData, $formName, $formSettings);

                if ( $files ) {
                    $this->clean_up_files( $files );
                }

                if ($mailchimp) {

                    $mailchimp_list_id = $currentForm['mailchimp']['list_id'];
                    $mailchimp_field_values = array();
                    $mailchimp_fields = $currentForm['mailchimp']['fields'];
                    $mailchimp_email = "";

                    if ($mailchimp_fields['email']) {
                        $mailchimp_email = $formData[$mailchimp_fields['email']];
                    }

                    foreach ($mailchimp_fields as $tag => $form_field) {
                        if (isset($formData[$form_field]) && !empty($formData[$form_field])) {
                            $mailchimp_field_values[$tag] = $formData[$form_field];
                        }
                    }

                    if (isset($currentForm['mailchimp']['trigger'])) {
                        if (!empty($mailchimp_email) && $formData[$currentForm['mailchimp']['trigger']] != "") {
                            $mailchimp_status = "subscribed";
                            if (isset($currentForm['mailchimp']['status'])) {
                                $mailchimp_status = $currentForm['mailchimp']['status'];
                            }
                            $this->mailchimp->addToList($mailchimp_email, $mailchimp_list_id, $mailchimp_field_values, $mailchimp_status);
                        }
                    }
                }

                return array(
                    'hasErrors' => false,
                    'successMessage' => $successMessage,
                    'redirect' => $redirect,
                    'redirect_new_tab' => $redirect_new_tab,
                    'success_html' => $success_html
                ); 

            } catch ( NestedValidationException $ex ) {

                $errorKey = array_keys( $tmpErrors );
                $errors = $ex->findMessages( $errorKey );
                $errors = $this->format_error_messages( $errors );

                $response = array (
                    'hasErrors' => true,
                    'errors' => $errors,
                    'mainError' => $errorMessage
                );

                if ($ex->getMessage() == "Invalid file") {
                    $response['mainError'] = 'The file you uploaded is not valid. Please check the file type and size.';
                }

                return $response;
            }
        }
    }

    public function process_files($files, $mimetypes, $filesize)
    {
        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
        }

        $keys = array_keys($files);

        for ($i = 0; $i < count($keys); $i++) {
            $file = $files[$keys[$i]];              // Current File
            $correctMime = false;                   // Flag for mime
            $correctSize = false;                   // Flag for size
            $uploadedFiles = array();               // Our uploaded files

            if ($mimetypes) {
                foreach ($mimetypes as $mimetype) {
                    if (Validator::mimetype($mimetype)->validate($file['tmp_name'])) {
                        $correctMime = true;
                        break;
                    }
                }
            } else {
                $correctMime = true;  
            }
            
            if ($filesize) {
                if (Validator::size(null, $filesize)->validate($file['tmp_name'])) {
                    $correctSize = true;
                }
            } else {
                $correctSize = true; 
            }

            if ($correctMime && $correctSize) { // If these be true, upload the file bruv
                $uploaded = wp_handle_upload($file, array('test_form' => false));

                if ($uploaded && !isset( $uploaded['error'])) {
                    $uploadedFiles[] = $uploaded['file'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return $uploadedFiles;
    }

    public function clean_up_files($files)
    {
        foreach ($files as $file) {
            unlink($file);
        }
    }

    public function process_data( $data, $fields, $formSlug )
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "gp_forms_entires";
        $keys = array_keys( $data );      

        $post_title = $data[$keys[0]];  // Set Post Title to first field
        if (isset($data['subject']) && !empty($data['subject'])) $post_title .= " - " . $data['subject'];

        $title = apply_filters('gp_forms_entry_title', $post_title, $data, $fields, $formSlug);

        $new_id = wp_insert_post(array(
            'post_type'   => 'gp_forms',
            'post_title'  => $title,
            'post_status' => 'publish'
        ));
        
        $postId = isset($data['post_id']) ? $data['post_id'] : null;

        wp_set_object_terms( $new_id, $formSlug, 'form' );

        if ( $new_id !== 0 ) {
            for ( $i = 0; $i < count($keys); $i++ ) {
                // don't save post_id as entry meta as it is saved in a column
                if ($keys[$i] == 'post_id') continue;

                $iv = openssl_random_pseudo_bytes( openssl_cipher_iv_length( 'aes-256-ctr' ) );
                $value = openssl_encrypt( $data[$keys[$i]], 'aes-256-ctr', AUTH_KEY, 0, $iv );
                $value = bin2hex( $iv ) . $value;

                $iv = openssl_random_pseudo_bytes( openssl_cipher_iv_length( 'aes-256-ctr' ) );                    
                $field = openssl_encrypt( $keys[$i], 'aes-256-ctr', AUTH_KEY, 0, $iv );
                $field = bin2hex( $iv ) . $field;

                $wpdb->insert( $table_name, array(
                    'form_id' => $formSlug,
                    'post_id' => $postId,
                    'entry_id' => $new_id,
                    'field' => $field,
                    'value' => $value
                ) );
            }

            return true;
        }

        return false;
    }

    public function notify_admin($data, $formName, $formSettings, $notificationEmail, $from, $files, $cc)
    {
        add_filter('wp_mail_content_type', array($this, "set_to_html"));

        if (isset($data['post_id'])) {
            unset($data['post_id']);
        }

        $labels = array();

        if (isset($formSettings['labels'])) {
            $labels = $formSettings['labels'];
        }
        
        $headers[] = "From: " . $from['name'] .  " <" . $from['email'] . ">";

        if ($cc !== "") {
            $headers[] = "CC: <" . $cc . ">" ;
        }

        $subject = "Website Form Submission - " . $formName;

        if (isset($data['subject']) && !empty($data['subject'])) {
            $subject .= " - " . $data['subject'];
        }

        // fixed typo: gp_forms_nofify_admin_subject
        $subject = apply_filters('gp_forms_notify_admin_subject', $subject, $data, $formName, $formSettings);

        $body = "";
        $keys = array_keys( $data );

        for ( $i = 0; $i < count($keys); $i++ ) {
            $label = (isset($labels[$keys[$i]])) ? $labels[$keys[$i]] : $keys[$i];
            $body .=  "<strong>" . $label . ":</strong> " . nl2br($data[$keys[$i]]) . "<br>";
        }

        // fixed typo: gp_forms_nofify_admin_body
        $body = apply_filters('gp_forms_notify_admin_body', $body, $data, $formName, $formSettings);

        if ($files) {
            wp_mail($notificationEmail, $subject, $body, $headers, $files);
        } else {
            wp_mail($notificationEmail, $subject, $body, $headers);                
        }

        remove_filter('wp_mail_content_type', array($this, "set_to_html"));
    }

    public function set_to_html()
    {
        return "text/html";
    }

    public function format_error_messages($errors)
    {
        $errorsFormatted = array();

        foreach ($errors as $key => $message) {
            // clean up email message
            $message = str_replace('be valid email', 'be a valid email', $message);
            // Replace first occurrence of field name in string with 'this field'.
            $pos = strpos($message, $key);
            if ($pos !== false) {
                $message = substr_replace($message, 'this field', $pos, strlen($key));
            }
            $errorsFormatted[$key] = ucfirst($message);
        }

        return $errorsFormatted;
    }

    protected function checkRecaptcha($recaptchaResponse, $remoteIp)
    {
        $siteUrl = get_site_url();
        $hostName = parse_url($siteUrl, PHP_URL_HOST);
        
        // error_log('$hostName: ' . $hostName);

        $recaptcha = new ReCaptcha($this->googleRecaptchaSecretKey);
        $response = $recaptcha->setExpectedHostname($hostName)->verify($recaptchaResponse, $remoteIp);

        if ($response->isSuccess()) {
            return true;
        } else {
            $errors = $response->getErrorCodes();
            //error_log('checkRecaptcha errors: '.print_r($errors, 1));
            return false;
        }
    }

    // http://itman.in/en/how-to-get-client-ip-address-in-php/
    protected function getRemoteIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}
