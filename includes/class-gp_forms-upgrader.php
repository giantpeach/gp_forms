<?php
/**
 * Fired during plugin upgrade.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.1
 * @package    Gp_forms
 * @subpackage Gp_forms/includes
 * @author     Giant Peach <support@giantpeach.agency>
 */
class Gp_forms_Upgrader {

	/**
	 * Version of plugin
	 */
	protected $version;

	public function __construct($version) {
		$this->version = $version;
	}

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.1
	 */
	public function upgrade() {
		$this->upgrade_db();
	}

	protected function upgrade_db() {

		global $wpdb;
		$plugin_version = $this->version;

		$old_version = (get_option( 'gpforms_plugin_version' )) ? get_option( 'gpforms_plugin_version' ) : '1.0.0';

		if ($plugin_version == $old_version) return;

		$table_name = $wpdb->prefix . "gp_forms_entires";
		$charset_collate = $wpdb->get_charset_collate();

		$sql = $this->get_update_sql("",$old_version,$table_name);
		if ($sql) {
			$wpdb->query($sql);
		}

		update_option( 'gpforms_plugin_version', $plugin_version );

	}

	public static function get_update_sql($sql, $old_version, $table_name) {

		if ($old_version < '1.0.1') {
			$sql .= "ALTER TABLE $table_name ADD post_id mediumint(8)";
        }

		return $sql;

	}

}
