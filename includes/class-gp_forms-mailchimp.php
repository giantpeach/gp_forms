<?php

/**
 * Gp_Forms_Mailchimp class
 */
class Gp_Forms_Mailchimp
{
    /**
     * The ID of this plugin.
     *
     * @since  1.0.0
     * @access private
     * @var    string    $plugin_name    The ID of this plugin.
     */
    private $_plugin_name;

    /**
     * The version of this plugin.
     *
     * @since  1.0.0
     * @access private
     * @var    string    $version    The current version of this plugin.
     */
    private $_version;

    private $_url;
    private $_api_key;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of the plugin.
     * @param string $version     The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->_plugin_name = $plugin_name;
        $this->_version = $version;

        include get_template_directory() . '/gp_forms/forms.php';

        if (isset($mailchimp)) {
            $this->url = $mailchimp['url'];
            $this->api_key = $mailchimp['key'];
        }
    }

    /**
     * Add To List function summary
     *
     * @param string $email   Users email address to subscribe
     * @param string $fname   Users Firstname
     * @param string $lname   Users Lastname
     * @param string $list_id ID of mailchimp list
     * 
     * @return true
     **/
    public function addToList($email, $list_id, $fields, $status = "subscribed")
    {

        $data = array(
            "email_address" => $email,
            "status" => $status, // subscribed or pending for double opt-in
            "merge_fields" => $fields
        );

        $data_string = json_encode($data);

        $ch = curl_init($this->url . "/lists/" . $list_id . "/members/" . hash("md5", strtolower($email)));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "anystring:" . $this->api_key);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );

        $result = curl_exec($ch);
    }

    /**
     * Remove From List function summary
     * 
     * @param string $email   Users email address to unsubscribe
     * @param string $list_id ID of mailchimp list
     * 
     * @return true
     */
    public function removeFromList($email, $list_id)
    {
        $ch = curl_init($this->url . "lists/" . $list_id . "/members/" . hash("md5", strtolower($email)));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "anystring:" . $this->api_key);

        $result = curl_exec($ch);
    }
}
