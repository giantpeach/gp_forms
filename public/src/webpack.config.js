const webpack = require("webpack");
const path = require("path");

config = {
    // cache: false, // temporary fix to make --watch work!
    entry: "./gp_forms.js",
    output: {
        filename: "../js/gp_forms.js"
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: "/node_modules/",
            use: {
                loader: "babel-loader",
                query: {
                    presets: ["es2015"]
                }
            }
        }]
    },
    plugins: []
};

module.exports = config;

if (process.env.NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({output: {comments: false}})
    )
}