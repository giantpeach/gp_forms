
import Form from "./components/form";

function ready(func) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        func();
    } else {
        document.addEventListener("DOMContentLoaded", func);
    }
}

function load() {
    var forms = document.querySelectorAll(".gp_form");
    var loadedForms = {};

    if (forms !== null) {
        for (var i=0; i < forms.length; i++) {
            loadedForms[forms[i].name] = new Form(forms[i]);
        }
    }
    
    document.addEventListener("pageLoad", function () {
        forms = document.querySelectorAll(".gp_form");
        if (forms !== null) {
            for (var i=0; i < forms.length; i++) {
                loadedForms[forms[i].name] = new Form(forms[i]);
            }
        }
    });
}

ready(load);
