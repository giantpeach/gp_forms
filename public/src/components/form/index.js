
import ajax from "@fdaciuk/ajax";

Math.easeInOutQuint = function (t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t * t * t * t + b;
    t -= 2;
    return c / 2 * (t * t * t * t * t + 2) + b;
};

// CustomEvent polyfill
(function() {
    if (typeof window.CustomEvent === "function") return false; //If not IE

    function CustomEvent(event, params) {
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
        };
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

function Form(form) {
    this.form = form;
    var that = this;

    if (this.form) {
        this.inputEls = this.form.querySelectorAll(".input");
        this.inputs = this.form.querySelectorAll("input");
        this.textareas = this.form.querySelectorAll("textarea");
        this.selects = this.form.querySelectorAll("select");
        this.checkboxes = this.form.querySelectorAll("[type=checkbox]");
        this.radios = this.form.querySelectorAll("[type=radio]");
        this.submitBtn = this.form.querySelector("[type=submit]");
        this.formName = this.form.getAttribute("name");
        this.form.addEventListener("submit", this.submit.bind(this), false);

        for (var i = 0; i < this.inputs.length; i++) {
            this.inputs[i].addEventListener("blur", function () {
                that.textChange(this);
            });
        }
        for (var i = 0; i < this.textareas.length; i++) {
            this.textareas[i].addEventListener("blur", function () {
                that.textChange(this);
            });
        }
        for (var i = 0; i < this.selects.length; i++) {
            this.selects[i].addEventListener("change", function () {
                that.selectChange(this);
            });
        }
        for (var i = 0; i < this.checkboxes.length; i++) {
            this.checkboxes[i].addEventListener("change", function () {
                that.selectChange(this);
            });
        }
        for (var i = 0; i < this.radios.length; i++) {
            this.radios[i].addEventListener("change", function () {
                that.selectChange(this);
            });
        }
    }
}

Form.prototype.textChange = function (el) {
    const parentInput = findAncestor(el, '.input');
    
    if (el.value == "") {
        el.classList.remove("is-filled");
    } else {
        el.classList.add("is-filled");
    }

    if (parentInput) {
        parentInput.classList.remove("is-error");
        var message = el.parentNode.querySelector('.input-message');
        if (message !== null) {
            message.innerHTML = "";
        }
    }
    
};

Form.prototype.selectChange = function (el) {

    const parentInput = findAncestor(el, '.input');

    if (parentInput) {
        parentInput.classList.remove("is-error");
        var message = parentInput.querySelector('.input-message');
        if (message !== null) {
            message.innerHTML = "";
        }
    }

};

Form.prototype.submit = function (e) {
    e.preventDefault();

    if (this.form.classList.contains('waiting')) {
        return;
    }
    
    this.submitBtn.classList.add('waiting');

    this.form.classList.add('waiting');

    var that = this;

    this.data = new FormData(this.form);

    var gRecaptchaSiteKey = this.form.getAttribute('data-grecaptcha-site-key');

    var formName = this.form.getAttribute('name');

    if (typeof grecaptcha !== 'undefined' && typeof gRecaptchaSiteKey !== 'undefined' && gRecaptchaSiteKey !== null) {
        var grecaptchaAction = formName.replace(/[\W_]+/g, "");
        grecaptcha.execute(gRecaptchaSiteKey, { action: grecaptchaAction }).then(function (token) {
            that.data.append('g-recaptcha-response', token);
            var input = document.getElementById(formName + '_' + 'g-recaptcha-response');
            if (input) {
                input.value = token;
            }
            doSubmit();
        });
    } else {
        doSubmit();
    }

    
    function doSubmit() {
        ajax({
            headers: {
                "content-type": null
            }
        }).post("/wp-json/" + nonce[0] + "/submit/" + formName + "?_wpnonce=" + nonce[1], that.data).always(function (response) {
            
            that.submitBtn.classList.remove('waiting');
            that.form.classList.remove('waiting');

            var alertZone = that.form.querySelector(".form__alert-zone");

            if (alertZone !== null) alertZone.innerHTML = "";

            if (response.hasErrors !== false) {

                if (typeof response.mainError == 'undefined') {
                    that.createAlert("danger", 'An unknown error occured. Please contact us for assistance.');
                } else {
                    that.createAlert("danger", response.mainError);

                    for (var obj in response.errors) {
                        if (response.errors[obj] !== "") {
                            var tmp = that.form.querySelector("[name='" + obj + "']");
                            if (!tmp) { // might be a checkbox
                                tmp = that.form.querySelector("[name='" + obj + "[]']");
                            }
                            if (tmp) {
                                var tmpParent = findAncestor(tmp, '.input');
                                if (tmpParent) {
                                    tmpParent.classList.add("is-error");
                                    var messageEl = tmpParent.querySelector(".input-message");
                                    if (messageEl !== null) {
                                        messageEl.innerHTML = response.errors[obj];
                                    }
                                }
                            }
                        }
                    }
                }
                that.scrollToAlert();
            } else {
                if (typeof ga == 'function') { 
                    ga(
                        'send',
                        'event',
                        'Forms', // category
                        'submission', // action
                        formName, // label
                    );
                } 
                
                if (typeof gtag == 'function') { 
                    gtag('event', 'submission', {
                        'event_category' : 'Form',
                        'event_label' : formName,
                    });
                }

                var ev = new CustomEvent("gp_form_submission",{
                    detail: {
                      'form': formName
                    }
                });
                document.dispatchEvent(ev);

                if (typeof response.redirect !== 'undefined' && response.redirect) {
                    if (typeof response.redirect_new_tab !== 'undefined' && (response.redirect_new_tab === "true" || response.redirect_new_tab === true)) {
                        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
                        if (isSafari) {
                            window.location = response.redirect;
                        }
                        else {
                            window.open(response.redirect, "_blank");
                        }
                    }
                    else {
                        window.location = response.redirect;
                        return;
                    }
                }

                that.form.classList.add('sent');
                that.form.reset();

                var forms_count = document.querySelectorAll(".gp_form").length;
                // If there is more than one form, check for a fancy alert specific to form. Else, use the only fancy element.
                var fancy = forms_count > 1 ? document.querySelector(".alert--fancy-"+formName) : document.querySelector(".alert--fancy");
                if (fancy) {
                    fancy.classList.add("is-visible");
                    if (typeof response.success_html !== 'undefined' && response.success_html != '') {
                        fancy.innerHTML = response.success_html;
                    }
                    that.form.style.display = "none";
                } 
                else {
                    that.createAlert("success", response.successMessage);
                    that.hideFields();
                    that.scrollToAlert();
                }
            }
        });
    }
};

Form.prototype.hideFields = function () {
    if (this.inputEls !== null) {
        for (var i=0; i < this.inputEls.length; i++) {
            this.inputEls[i].classList.add('hide');
        }
    }
    if (this.submitBtn !== null) {
        this.submitBtn.classList.add('hide');
    }
}

Form.prototype.createAlert = function (type, message) {
    var alertZone = this.form.querySelector(".form__alert-zone");
    if (alertZone) {
        var alert = document.createElement("div");
        alert.setAttribute("class", "alert alert--" + type);
        alert.setAttribute("role", "alert");
        alert.innerHTML = message;
        if (this.form.dataset.alertclosebutton) {
                alert.classList.add("alert-dismissable", "fade",  "show");
            var closeButton = document.createElement("button");
                closeButton.setAttribute("class", "close");
                closeButton.setAttribute("data-dismiss", "alert");
                closeButton.setAttribute("aria-label", "Close");
            var span = document.createElement("span");
                span.setAttribute("aria-hidden", "true");
                span.innerHTML = "&times;";
            closeButton.appendChild(span);
            alert.appendChild(closeButton);
        }
        alertZone.appendChild(alert);
    }
};

Form.prototype.scrollToAlert = function() {
    // if form is in overlay, scroll overlay to top
    var offset = 150;
    var header = document.querySelector('header, .header');
    if (header && getComputedStyle(header).getPropertyValue('position') == 'fixed') {
        offset += header.offsetHeight;
    }
    var overlayContainer = findAncestor(this.form,'.overlay__content');
    if (overlayContainer.classList.contains('overlay__content')) {
        this.scroll(overlayContainer.scrollTop, 0, 2, overlayContainer);
    }
    // otherwise, scroll window to notification (with 150 offset)
    else {
        this.scroll(window.pageYOffset, this.form.getBoundingClientRect().top + window.pageYOffset - offset, 2);
    }
}

Form.prototype.scroll = function (from, to, duration, elementToScroll = window) {

    if (typeof from == 'undefined' || isNaN(from)) {
        return;
    }

    if (typeof to == 'undefined' || isNaN(to)) {
        return;
    }
    document.querySelector('body').classList.add('scrolling');
    if (typeof document.documentElement.style.scrollBehavior !== "undefined") {
        document.documentElement.style.scrollBehavior = "auto";
    }

    var change = to - from;
    var currentIteration = 0;
    var totalIterations = duration * 60;

    function animate() {
        currentIteration++;
        elementToScroll.scrollTo(0, Math.easeInOutQuint(currentIteration, from, change, totalIterations));

        if (currentIteration < totalIterations) {
            requestAnimationFrame(animate);
        }
        else {
            document.querySelector('body').classList.remove('scrolling');
            if (typeof document.documentElement.style.scrollBehavior !== "undefined") {
                document.documentElement.style.scrollBehavior = "smooth";
            }
        }
    }

    requestAnimationFrame(animate);
};

Form.prototype.processFields = function () {
    var i = 0;

    for (i = 0; i < this.inputs.length; i++) {
        this.data.append(this.inputs[i].getAttribute("name"), this.inputs[i].getAttribute("value"));
    }
};

// https://developer.mozilla.org/en-US/docs/Web/API/Element/matches
if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
}

function findAncestor (el, sel) {
    var parentEl = el;
    while ((parentEl = parentEl.parentElement) && !((parentEl.matches || parentEl.matchesSelector).call(parentEl,sel)));
    if (!parentEl) {
        parentEl = el.parentElement;
    }
    return parentEl;
}

export default Form;